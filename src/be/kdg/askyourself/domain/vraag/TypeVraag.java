package be.kdg.askyourself.domain.vraag;

/**
 * @author Jan de Rijke.
 */
public enum TypeVraag {
	OPEN,
	KEUZE
}
