package be.kdg.askyourself.application;

import be.kdg.askyourself.domain.persoon.Gebruiker;
import be.kdg.askyourself.domain.vraag.*;
import be.kdg.askyourself.persistence.GebruikersCataloog;
import be.kdg.askyourself.persistence.OnderwerpenCataloog;
import be.kdg.common.CryptUtils;

import java.time.LocalDate;

/**
 * @author Jan de Rijke.
 */
public class Application {
	private static final String DOMAIN = "kdg.be";
	private static String[][] gebruikerData = new String[][]{
		{"Christel", "Coenen"},
		{"Eddy", "Pijl"},
		{"Jan", "de Rijke"}
	};

	private static String[][] onderwerpData = new String[][]{
		{"SE1-1", "Software Engineering 1, Analyse"},
		{"SE1-2", "Software Engineering 1, Ontwerp"},
	};


	public static void main(String[] args) {
		// C001 initialisatie van  systeem structuur: creatie catalogen en verbinden aan systeem
		AskYourself system = new AskYourself();
		// initialisatie data
		initData(system);
		// C002 initialisatie van een sessie
		VragenlijstController ctrl = system.initSessie();
		// COO3 inloggen in sessie door gebruiker
		ctrl.logIn("jan.derijke@kdg.be","naj");
		// start use case
		testMaakNieuweVragenLijst(ctrl);
	}

	private static void testMaakNieuweVragenLijst(VragenlijstController ctrl) {
		// operatiecontract C01 maak nieuwe vragelijst
		Vragenlijst lijst = ctrl.maakVragenlijst("SE1-2","Test GRASP principes", ToegangStatus.PUBLIC);
		// operatiecontract C02 maak nieuwe vraag (+ C03 maak status), meerdere keren (in loop in SSD)
		ctrl.maakVraag("Duid de evaluerende GRASP principes aan", TypeVraag.KEUZE,2.0,true);
		// operatiecontract C04 maak antwoord (min geneste loop in SSD)
		ctrl.maakAntwoord("Controller",false);
		ctrl.maakAntwoord("Lage koppeling",true);
		ctrl.maakAntwoord("Hoge Cohesie",true);
		ctrl.maakAntwoord("Creator",false);
		ctrl.maakAntwoord("Information Expert",false);
		ctrl.maakVraag("Het creator GRASP principe zegt je in welke klasse je een constructor moet schrijven",
			TypeVraag.KEUZE,1.0,true);
		ctrl.maakAntwoord("Juist",false);
		ctrl.maakVraag("Wat is het effect van de toepassing van Information Expert op de koppeling?",
			TypeVraag.OPEN,1.0,false);
		// operatiecontract C05: beëindig vragenlijst
		System.out.println("Te verdienen punten: " +  ctrl.beeindigVragenlijst());
		System.out.println( lijst);
	}


	private static void initData(AskYourself system) {
		initGebruikers(system.getGebruikers());
		initOnderwerpen(system.getOnderwerpen());

	}

	private static void initOnderwerpen(OnderwerpenCataloog cataloog) {
		for (String[] data : onderwerpData) {
			cataloog.put(data[0], new Onderwerp(data[0], data[1]));
		}
	}

	private static void initGebruikers(GebruikersCataloog cataloog) {
		Gebruiker item;
		for (String[] data : gebruikerData) {
			item = initWerknemer(data[0], data[1]);
			cataloog.put(item.getUserName(), item);
		}
	}

	private static Gebruiker initWerknemer(String voornaam, String achternaam) {
		String email = String.format("%s.%s@%s", voornaam, achternaam.replaceAll("[^A-z]", ""), DOMAIN).toLowerCase();
		return new Gebruiker(
			email,
			voornaam,
			achternaam,
			email,
			LocalDate.now().minusYears(voornaam.length()),
			CryptUtils.toSHA1(new StringBuilder(voornaam.toLowerCase()).reverse().toString()));
	}
}