package be.kdg.askyourself.domain.vraag;

import be.kdg.askyourself.domain.persoon.Gebruiker;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jan de Rijke.
 */
public class Vragenlijst {
	private String naam;
	private ToegangStatus toegankelijkheid;
	private Gebruiker eigenaar;
	private List<Vraag> vragen = new ArrayList<>();

	public Vragenlijst(String naam, ToegangStatus toegang, Gebruiker gebruiker) {
		this.naam=naam;
		this.toegankelijkheid=toegang;
		this.eigenaar=gebruiker;
		gebruiker.voegLijstToe(this);
	}

	public String getNaam() {
		return naam;
	}

	public ToegangStatus getToegankelijkheid() {
		return toegankelijkheid;
	}

	public Gebruiker getEigenaar() {
		return eigenaar;
	}

	public Vraag maakVraag(String tekst, TypeVraag type, double score, boolean actief) {
		Vraag vraag = new Vraag(tekst,type,score,actief);
		vragen.add(vraag);
		return vraag;

	}

	public double getMaxScore() {
		double score = 0.0;
		for (Vraag vraag : vragen){
			if (vraag.isActief()){
				score += vraag.getScore();
			}
		}
		return score;
	}

	public int getAantalActief() {
		int actief = 0;
		for (Vraag vraag : vragen){
			if (vraag.isActief()){
				actief++;
			}
		}
		return actief;
	}

	@Override
	public String toString() {
		return "Vragenlijst{" +
			"naam='" + naam + '\'' +
			", toegankelijkheid=" + toegankelijkheid +
			", eigenaar=" + eigenaar +
			", vragen=\n" + vragen +
			'}';
	}
}