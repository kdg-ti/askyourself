package be.kdg.askyourself.domain.vraag;

/**
 * @author Jan de Rijke.
 */
public enum ToegangStatus {
	PUBLIC,
	PRIVATE,
	LIMITED,
	AUTHORISED
}