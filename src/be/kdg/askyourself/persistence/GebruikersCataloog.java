package be.kdg.askyourself.persistence;

import be.kdg.askyourself.domain.persoon.Gebruiker;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jan de Rijke.
 */
public class GebruikersCataloog {
	private Map<String, Gebruiker> items = new HashMap<>();


	public void put(String id,Gebruiker item) {
		items.put(id, item);
	}

	public Gebruiker get(String id) {
		return items.get(id);
	}
}