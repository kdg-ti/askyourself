package be.kdg.askyourself.application;

import be.kdg.askyourself.persistence.GebruikersCataloog;
import be.kdg.askyourself.persistence.OnderwerpenCataloog;

/**
 * @author Jan de Rijke.
 */
class AskYourself {
	private GebruikersCataloog gebruikers = new GebruikersCataloog();
	private OnderwerpenCataloog onderwerpen = new OnderwerpenCataloog();

	GebruikersCataloog getGebruikers() {
		return gebruikers;
	}

	OnderwerpenCataloog getOnderwerpen() {
		return onderwerpen;
	}

	VragenlijstController initSessie(){
		return  new VragenlijstController(gebruikers,onderwerpen);
	}
}