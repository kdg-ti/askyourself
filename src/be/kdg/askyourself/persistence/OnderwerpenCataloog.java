package be.kdg.askyourself.persistence;

import be.kdg.askyourself.domain.vraag.Onderwerp;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jan de Rijke.
 */
public class OnderwerpenCataloog {
		private Map<String, Onderwerp> items = new HashMap<>();


		public void put(String id, Onderwerp item) {
			items.put(id, item);
		}

		public Onderwerp get(String id) {
			return items.get(id);
		}
	}
