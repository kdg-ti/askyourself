package be.kdg.askyourself.domain.persoon;

import be.kdg.askyourself.domain.vraag.Vragenlijst;
import be.kdg.common.CryptUtils;

import java.time.LocalDate;
import java.util.*;

/**
 * @author Jan de Rijke.
 */
public class Gebruiker {
	private String userName;
	private String voornaam;
	private String achternaam;
	private String email;
	private LocalDate lidSinds;
	private String wachtwoord;
	private Map<String,Vragenlijst> vragenlijsten = new HashMap<>();

	public Gebruiker(
		String userName,
		String voornaam,
		String achternaam,
		String email,
		LocalDate lidSinds, String wachtwoord) {
		this.userName = userName;
		this.voornaam = voornaam;
		this.achternaam = achternaam;
		this.email = email;
		this.lidSinds = lidSinds;
		this.wachtwoord = wachtwoord;
	}

	public boolean controleerWachtwoord(String wachtwoord) {
		return this.wachtwoord.equals(CryptUtils.toSHA1(wachtwoord));
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getVoornaam() {
		return voornaam;
	}

	public void setVoornaam(String voornaam) {
		this.voornaam = voornaam;
	}

	public String getAchternaam() {
		return achternaam;
	}

	public void setAchternaam(String achternaam) {
		this.achternaam = achternaam;
	}

	public String getEmail() {
		return email;
	}

	public void voegLijstToe(Vragenlijst vragenlijst) {
		vragenlijsten.put(vragenlijst.getNaam(), vragenlijst);
	}

	@Override
	public String toString() {
		return "Gebruiker{" +
			"userName='" + userName + '\'' +
			", voornaam='" + voornaam + '\'' +
			", achternaam='" + achternaam + '\'' +
			", email='" + email + '\'' +
			", lidSinds=" + lidSinds +
			'}';
	}
}