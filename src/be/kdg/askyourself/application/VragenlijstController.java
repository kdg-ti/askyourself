package be.kdg.askyourself.application;

import be.kdg.askyourself.domain.persoon.Gebruiker;
import be.kdg.askyourself.domain.vraag.*;
import be.kdg.askyourself.persistence.GebruikersCataloog;
import be.kdg.askyourself.persistence.OnderwerpenCataloog;

/**
 * @author Jan de Rijke.
 */
public class VragenlijstController {
	private final OnderwerpenCataloog oc;
	private final GebruikersCataloog gc;
	private  Gebruiker sessieGebruiker;
	private Vragenlijst sessieVragenLijst;
	private Vraag sessieVraag;

	public VragenlijstController(
		GebruikersCataloog gebruikers,
		OnderwerpenCataloog onderwerpen) {
		this.gc = gebruikers;
		this.oc =onderwerpen;
	}

	public boolean logIn(String username, String wachtwoord) {
		Gebruiker gebruiker = gc.get(username);
		if( gebruiker != null && gebruiker.controleerWachtwoord(wachtwoord)){
			sessieGebruiker = gebruiker;
			return true;
		};
		return false;
	}


	public Vragenlijst maakVragenlijst(
		String onderwerpKorteBeschrijving,
		String naam,
		ToegangStatus toegang) {
		sessieVragenLijst= oc.get(onderwerpKorteBeschrijving).voegLijstToe(naam,toegang, sessieGebruiker);
		return sessieVragenLijst;
	}

	public Vraag maakVraag(String tekst, TypeVraag type,double score, boolean actief){
		sessieVraag = sessieVragenLijst.maakVraag(tekst,type,score,actief);
		return sessieVraag;
	}

	public void maakAntwoord(String tekst,boolean isJuist){
		sessieVraag.voegAntwoordToe(tekst,isJuist);
	}

	public double beeindigVragenlijst (){
		double score = sessieVragenLijst.getMaxScore();
		sessieVraag = null;
		sessieVragenLijst = null;
		return score;

	}
}