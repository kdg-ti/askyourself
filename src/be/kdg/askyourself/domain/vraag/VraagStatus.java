package be.kdg.askyourself.domain.vraag;

import java.time.LocalDateTime;

/**
 * @author Jan de Rijke.
 */
public class VraagStatus {
	boolean actief;
	LocalDateTime timestamp;

	public VraagStatus(boolean actief) {
		this.actief = actief;
		timestamp=LocalDateTime.now();
	}

	public boolean isActief() {
		return actief;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}
}
