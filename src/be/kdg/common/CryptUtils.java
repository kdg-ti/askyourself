package be.kdg.common;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

/**
 * @author Jan de Rijke.
 */
public class CryptUtils {

	private static String HASH="SHA";
	public static String toSHA1(String convertme) {
		try {
			return Base64.getEncoder().encodeToString((
				MessageDigest.getInstance(HASH).digest(convertme.getBytes(StandardCharsets.UTF_8))));
		} catch (NoSuchAlgorithmException e) {
			System.err.println("Error encrypting String " +  e);
			return null;
		}
	}
}