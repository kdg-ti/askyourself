package be.kdg.askyourself.domain.vraag;

import be.kdg.askyourself.domain.persoon.Gebruiker;

import java.util.HashMap;
import java.util.Map;

/**
 * @author Jan de Rijke.
 */
public class Onderwerp {
	private String korteBeschrijving;
	private String langeBeschrijving;
	private Map<String,Vragenlijst> vragenlijsten = new HashMap<>();


	public Onderwerp(String korteBeschrijving, String langeBeschrijving) {
		this.korteBeschrijving = korteBeschrijving;
		this.langeBeschrijving = langeBeschrijving;
	}

	public String getKorteBeschrijving() {
		return korteBeschrijving;
	}

	public String getLangeBeschrijving() {
		return langeBeschrijving;
	}

	public Vragenlijst voegLijstToe(String naam, ToegangStatus toegang, Gebruiker gebruiker) {
		Vragenlijst vl =  new Vragenlijst(naam,toegang,gebruiker);
		vragenlijsten.put(naam, new Vragenlijst(naam,toegang,gebruiker));
		return vl;
	}
}