package be.kdg.askyourself.domain.vraag;

/**
 * @author Jan de Rijke.
 */
public class Antwoord {
	String tekst;
	boolean isJuist;

	public Antwoord(String tekst, boolean isJuist) {
		this.tekst = tekst;
		this.isJuist = isJuist;
	}

	@Override
	public String toString() {
		return "Antwoord{" +
			"tekst='" + tekst + '\'' +
			", isJuist=" + isJuist +
			'}';
	}
}