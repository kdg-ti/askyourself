package be.kdg.askyourself.domain.vraag;

import java.util.*;

/**
 * @author Jan de Rijke.
 */
public class Vraag {
	private String tekst;
	private TypeVraag type;
	private double score;
	private Deque<VraagStatus> vraagStatussen =new ArrayDeque<>();
	private List<Antwoord> antwoorden=new ArrayList<>();

	public Vraag(String tekst, TypeVraag type, double score, boolean status) {
		this.tekst = tekst;
		this.type = type;
		this.score = score;
		vraagStatussen.add(new VraagStatus(status));
	}

	public Antwoord voegAntwoordToe(String tekst, boolean isJuist) {
		Antwoord antwoord = new Antwoord(tekst,isJuist);
		antwoorden.add(new Antwoord(tekst,isJuist));
		return antwoord;
	}

	public boolean isActief(){
		return vraagStatussen.getLast().isActief();
	}


	public String getTekst() {
		return tekst;
	}

	public TypeVraag getType() {
		return type;
	}

	public double getScore() {
		return score;
	}

	public List<Antwoord> getAntwoorden() {
		return antwoorden;
	}

	@Override
	public String toString() {
		return "Vraag{" +
			"tekst='" + tekst + '\'' +
			", type=" + type +
			", score=" + score +
			", vraagStatussen=" + isActief() +
			", antwoorden=" + antwoorden +
			"}\n";
	}
}